<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <div id = "filePick">
      <h1>Upload zip file form!</h1>
    	<form action="" method="post"  enctype="multipart/form-data">
    		<input type="file" name="zip"/>
    		<input type="submit" name="submit" onclick="wait()"/>
    	</form>
    </div>
    <div id="waitMessage" style="display:none">
      <h1>Upload in progress...</h1>
    </div>
    <script type="text/javascript">
      function wait() {
        document.getElementById("filePick").style.display = "none";
        document.getElementById("waitMessage").style.display = "block";
      }
    </script>
    <?php
      require_once('./db.php');
       if(isset($_FILES['zip'])){
          $errors= array();
          $file_name = $_FILES['zip']['name'];
          $file_size =$_FILES['zip']['size'];
          $file_tmp =$_FILES['zip']['tmp_name'];
          $file_type=$_FILES['zip']['type'];
          $file_ext = strtolower(end(explode('.',$_FILES['zip']['name'])));
          $extensions= array("zip");
          if(in_array($file_ext,$extensions)=== false){
             $errors[]="File type not allowed, please choose a zip file.";
          }
          if(empty($errors)==true){
            if(!file_exists("uploads"))
          		mkdir("uploads");
             move_uploaded_file($file_tmp,"uploads/".$file_name);
             error_log("File uploaded successfully!");
             importSnapshot();
             echo "<script>location.href = 'timetable.php'</script>";
          } else
             error_log(explode(" ", $errors));
       }
       function importSnapshot() {
       // Unzip uploaded file
       	$zip = new ZipArchive;
       	$res = $zip->open('uploads/timetableJSON.zip');
       	if ($res === TRUE) {
         		$zip->extractTo('uploads/');
         		$zip->close();
         		error_log('File extracted successfully!');
       	}	else {
         		error_log('File extraction failed!');
       	}
       //Read directory and store what all files are present
       	$dir = "uploads/timetableJSON/";
       	if(!is_dir($dir)) {
       		error_log("Directory uploads/timetableJSON does not exist");
       	}
       	$files = [];
       	if($dh = opendir($dir)) {
       		while(($file = readdir($dh)) !== false) {
       			array_push($files, $file);
       		}
       		closedir($dh);
       		error_log("Files in uploads/timetableJSON = " . implode(" ", $files));
       	}
       //Extract table names from files in array table1
       	$table1 = [];
       	foreach ($files as $key) {
       		if($key == "." || $key == "..") {
       			continue;
       		}
       		$table = strtok($key, ".");
       		array_push($table1, $table);
       	}
       //arrange table1 in required sequence into new array called table
       	$sequence = ["teacherReadable", "subject", "class", "batch", "room", "batchClassReadable", "batchCanOverlapReadable", "subjectClassTeacherReadable", "subjectBatchTeacherReadable", "overlappingSBTReadable", "timeTableReadable", "fixedEntry"];
       	$tables = [];
       	foreach ($sequence as $key) {
       		if(in_array($key, $table1))
       			array_push($tables, $key);
       	}
       	error_log("Tables in required sequence = ".implode(" ", $tables));
       //Check if import is possible or not
       	$import = 1;
       	if(in_array("batchClassReadable", $tables) == true)
       		if((in_array("batch", $tables) == false) || (in_array("class", $tables) == false))
       			$import = 0;
       	if(in_array("batchCanOverlapReadable", $tables) == true)
       		if(in_array("batch", $tables) == false)
       			$import = 0;
       	if(in_array("subjectClassTeacherReadable", $tables) == true)
       		if((in_array("subject", $tables) == false) || (in_array("class", $tables) == false) || (in_array("teacherReadable", $tables) == false))
       			$import= 0;
       	if(in_array("subjectBatchTeacherReadable", $tables) == true)
       		if((in_array("subject", $tables) == false) || (in_array("batch", $tables)== false) || (in_array("teacherReadable", $tables) == false))
       			$import= 0;
       	if(in_array("overlappingSBTReadable", $tables) == true)
       		if(in_array("subjectBatchTeacherReadable", $tables) == false)
       			$import = 0;
       	if(in_array("timeTableReadable", $tables) == true)
       		if((in_array("room", $tables) == false) || (in_array("class", $tables) == false) || (in_array("subject", $tables) == false) || (in_array("teacherReadable", $tables) == false) || (in_array("batch", $tables) == false))
       			$import = 0;
       	if(in_array("fixedEntry", $tables) == true)
       		if(in_array("timeTableReadable", $tables) == false)
       			$import = 0;
       	if($import == 0) {
       		error_log("Import failed because files missing");
       	} else {
       		error_log("Import possible");
       //insert new entry in snapshot table
       		$query = "select max(snapshotId) from snapshot;";
       		$row = sqlGetOneRow($query);
       		$max = implode(" ", $row[0]);
       		$max = number_format($max);
       		$snapshotId = $max + 1;
       		$snapshotId = strval($snapshotId);
       		$snapshotName = "snap".$snapshotId;
       		$time = date("h:i:s");
       		$query = "insert into snapshot values ('".$snapshotId."', '".$snapshotName."', '1', '".$time."', '".$time."', '1');";
       		$result =	sqlUpdate($query);
       		if($result == false)
       			error_log("Query failed = ".$query);
       		$primary = array('teacher' => 0, 'subject' => 0,'class' => 0,'batch' => 0,'room' => 0);
       //loop through each table and update it with new snapshot
       		foreach($tables as $table) {
       //update primary table teacherReadable
       			if($table == "teacherReadable") {
       				error_log("Inside primary case teacherReadable");
       				$fileName = $table.".json";
       				$file = fopen("uploads/timetableJSON/".$fileName, 'r') or die('Unable to open file!');
       				while(!feof($file)) {
       					$array = [];
       					array_push($array, "null");
       					$tuple = json_decode(fgets($file), true);
       					if(($tuple == NULL) || (empty($tuple) == true)) {
       						continue;
       					}
       					foreach ($tuple as $key => $value) {
       						if($key == "deptShortName")
       							$value = 1;
       						array_push($array, $value);
       					}
       					array_push($array, $snapshotId);
       					tableInsert("teacher", $array);
       				}
       			}
       //update primary tables subject, class, batch, room
       			if(($table == "subject") || ($table == "class") || ($table == "batch") || ($table == "room")) {
       				error_log("Inside primary case ".$table);
       				$fileName = $table.".json";
       				$file = fopen("uploads/timetableJSON/".$fileName, 'r') or die('Unable to open file!');
       				while(!feof($file)) {
       					$array = [];
       					array_push($array, "null");
       					$tuple = json_decode(fgets($file), true);
       					if(($tuple == NULL) || (empty($tuple) == true)) {
       						continue;
       					}
       					foreach ($tuple as $key => $value) {
       						array_push($array, $value);
       					}
       					array_push($array, $snapshotId);
       					tableInsert($table, $array);
       				}
       			}
       //update secondary table batchClass
       			if($table == "batchClassReadable") {
       				error_log("Inside secondary case batchClassReadable");
       				if($primary["batch"] == 0) {
       					$associativeBatch = idNameMapping("batch", "batchName", "batchId", $snapshotId);
       					$primary["batch"] = 1;
       				}
       				$s = "";
       				foreach ($associativeBatch as $key => $value) {
       					$s .= $key." => ".$value.", ";
       				}
       //				error_log("associativeBatch = ".$s);
       				if($primary["class"] == 0) {
       					$associativeClass = idNameMapping("class", "classShortName", "classId", $snapshotId);
       					$primary["class"] = 1;
       				}
       				$s = "";
       				foreach ($associativeClass as $key => $value) {
       					$s .= $key." => ".$value.", ";
       				}
       //				error_log("associativeClass = ".$s);
       				$fileName = $table.".json";
       				$file = fopen("uploads/timetableJSON/".$fileName, 'r') or die('Unable to open file!');
       				while(!feof($file)) {
       					$array = [];
       					array_push($array, "null");
       					$tuple = json_decode(fgets($file), true);
       					if(($tuple == NULL) || (empty($tuple) == true)) {
       						continue;
       					}
       					foreach ($tuple as $key => $value) {
       						if($key == "batchName") {
       							$value = $associativeBatch[$value];
       						}
       						if($key == "classShortName") {
       							$value = $associativeClass[$value];
       						}
       						array_push($array, $value);
       					}
       					array_push($array, $snapshotId);
       					tableInsert("batchClass", $array);
       				}
       			}
       //update secondary table batchCanOverlap
       			if($table == "batchCanOverlapReadable") {
       				error_log("Inside secondary case batchCanOverlapReadable");
       				if($primary["batch"] == 0) {
       					$associativeBatch = idNameMapping("batch", "batchName", "batchId", $snapshotId);
       					$primary["batch"] = 1;
       				}
       				$s = "";
       				foreach ($associativeBatch as $key => $value) {
       					$s .= $key." => ".$value.", ";
       				}
       //				error_log("associativeBatch = ".$s);
       				$fileName = $table.".json";
       				$file = fopen("uploads/timetableJSON/".$fileName, 'r') or die('Unable to open file!');
       				while(!feof($file)) {
       					$array = [];
       					array_push($array, "null");
       					$tuple = json_decode(fgets($file), true);
       					if(($tuple == NULL) || (empty($tuple) == true)) {
       						continue;
       					}
       					foreach ($tuple as $key => $value) {
       						if(($key == "b1Name") || ($key == "b2Name")) {
       							$value = $associativeBatch[$value];
       						}
       						array_push($array, $value);
       					}
       					array_push($array, $snapshotId);
       					tableInsert("batchCanOverlap", $array);
       				}
       			}
       //update secondary table subjectClassTeacher
       			if($table == "subjectClassTeacherReadable") {
       				error_log("Inside secondary case subjectClassTeacherReadable");
       				if($primary["subject"] == 0) {
       					$associativeSubject = idNameMapping("subject", "subjectShortName", "subjectId", $snapshotId);
       					$primary["subject"] = 1;
       				}
       				$s = "";
       				foreach ($associativeSubject as $key => $value) {
       					$s .= $key." => ".$value.", ";
       				}
       //				error_log("associativeSubject = ".$s);
       				if($primary["class"] == 0) {
       					$associativeClass = idNameMapping("class", "classShortName", "classId", $snapshotId);
       					$primary["class"] = 1;
       				}
       				$s = "";
       				foreach ($associativeClass as $key => $value) {
       					$s .= $key." => ".$value.", ";
       				}
       //				error_log("associativeClass = ".$s);
       				if($primary["teacher"] == 0) {
       					$associativeTeacher = idNameMapping("teacher", "teacherShortName", "teacherId", $snapshotId);
       					$primary["teacher"] = 1;
       				}
       				$s = "";
       				foreach ($associativeTeacher as $key => $value) {
       					$s .= $key." => ".$value.", ";
       				}
       //				error_log("associativeTeacher = ".$s);
       				$fileName = $table.".json";
       				$file = fopen("uploads/timetableJSON/".$fileName, 'r') or die('Unable to open file!');
       				while(!feof($file)) {
       					$array = [];
       					array_push($array, "null");
       					$tuple = json_decode(fgets($file), true);
       					if(($tuple == NULL) || (empty($tuple) == true)) {
       						continue;
       					}
       					foreach ($tuple as $key => $value) {
       						if($key == "subjectShortName") {
       							$value = $associativeSubject[$value];
       						}
       						if($key == "classShortName") {
       							$value = $associativeClass[$value];
       						}
       						if($key == "teacherShortName") {
       							$value = $associativeTeacher[$value];
       						}
       						array_push($array, $value);
       					}
       					array_push($array, $snapshotId);
       					tableInsert("subjectClassTeacher", $array);
       				}
       			}
       //update secondary table subjectBatchTeacher
       			if($table == "subjectBatchTeacherReadable") {
       				error_log("Inside secondary case subjectBatchTeacherReadable");
       				if($primary["subject"] == 0) {
       					$associativeSubject = idNameMapping("subject", "subjectShortName", "subjectId", $snapshotId);
       					$primary["subject"] = 1;
       				}
       				$s = "";
       				foreach ($associativeSubject as $key => $value) {
       					$s .= $key." => ".$value.", ";
       				}
       //			error_log("associativeSubject = ".$s);
       				if($primary["batch"] == 0) {
       					$associativeBatch = idNameMapping("batch", "batchName", "batchId", $snapshotId);
       					$primary["batch"] = 1;
       				}
       				$s = "";
       				foreach ($associativeBatch as $key => $value) {
       					$s .= $key." => ".$value.", ";
       				}
       //				error_log("associativeBatch = ".$s);
       				if($primary["teacher"] == 0) {
       					$associativeTeacher = idNameMapping("teacher", "teacherShortName", "teacherId", $snapshotId);
       					$primary["teacher"] = 1;
       				}
       				$s = "";
       				foreach ($associativeTeacher as $key => $value) {
       					$s .= $key." => ".$value.", ";
       				}
       //				error_log("associativeTeacher = ".$s);
       				$fileName = $table.".json";
       				$file = fopen("uploads/timetableJSON/".$fileName, 'r') or die('Unable to open file!');
       				while(!feof($file)) {
       					$array = [];
       					array_push($array, "null");
       					$tuple = json_decode(fgets($file), true);
       					if(($tuple == NULL) || (empty($tuple) == true)) {
       						continue;
       					}
       					foreach ($tuple as $key => $value) {
       						if($key == "subjectShortName") {
       							$value = $associativeSubject[$value];
       						}
       						if($key == "batchName") {
       							$value = $associativeBatch[$value];
       						}
       						if($key == "teacherShortName") {
       							$value = $associativeTeacher[$value];
       						}
       						array_push($array, $value);
       					}
       					array_push($array, $snapshotId);
       					tableInsert("subjectBatchTeacher", $array);
       				}
       			}
       //update secondary table overlappingSBT
       			if($table == "overlappingSBTReadable") {
       				error_log("Inside secondary case overlappingSBTReadable");
       				if($primary["subject"] == 0) {
       					$associativeSubject = idNameMapping("subject", "subjectShortName", "subjectId", $snapshotId);
       					$primary["subject"] = 1;
       				}
       				$s = "";
       				foreach ($associativeSubject as $key => $value) {
       					$s .= $key." => ".$value.", ";
       				}
       //				error_log("associativeSubject = ".$s);
       				if($primary["batch"] == 0) {
       					$associativeBatch = idNameMapping("batch", "batchName", "batchId", $snapshotId);
       					$primary["batch"] = 1;
       				}
       				$s = "";
       				foreach ($associativeBatch as $key => $value) {
       					$s .= $key." => ".$value.", ";
       				}
       //				error_log("associativeBatch = ".$s);
       				if($primary["teacher"] == 0) {
       					$associativeTeacher = idNameMapping("teacher", "teacherShortName", "teacherId", $snapshotId);
       					$primary["teacher"] = 1;
       				}
       				$s = "";
       				foreach ($associativeTeacher as $key => $value) {
       					$s .= $key." => ".$value.", ";
       				}
       //				error_log("associativeTeacher = ".$s);
       				$fileName = $table.".json";
       				$file = fopen("uploads/timetableJSON/".$fileName, 'r') or die('Unable to open file!');
       				while(!feof($file)) {
       					$array = [];
       					array_push($array, "null");
       					$tuple = json_decode(fgets($file), true);
       					if(($tuple == NULL) || (empty($tuple) == true)) {
       						continue;
       					}
       					$arr = array();
       					foreach ($tuple as $key => $value) {
       						array_push($arr, $value);
       					}
       //					error_log("arr = ".implode(" ", $arr));
       					$query1 = "select sbtId from subjectBatchTeacher where subjectId=".$associativeSubject[$arr[0]]." and batchId=".$associativeBatch[$arr[1]]." and teacherId=".$associativeTeacher[$arr[2]]." ;";
       					$query2 = "select sbtId from subjectBatchTeacher where subjectId=".$associativeSubject[$arr[3]]." and batchId=".$associativeBatch[$arr[4]]." and teacherId=".$associativeTeacher[$arr[5]]." ;";
       					$result = sqlGetOneRow($query1);
       					$sbtId1 = implode(" ", $result[0]);
       					$result = sqlGetOneRow($query2);
       					$sbtId2 = implode(" ", $result[0]);
       					array_push($array, $sbtId1);
       					array_push($array, $sbtId2);
       					array_push($array, $snapshotId);
       					tableInsert("overlappingSBT", $array);
       				}
       			}
       //update tertiory table timeTable
       			if($table == "timeTableReadable") {
       				error_log("Inside tertiory case timeTableReadable");
       				if($primary["room"] == 0) {
       					$associativeRoom = idNameMapping("room", "roomShortName", "roomId", $snapshotId);
       					$primary["room"] = 1;
       				}
       				$s = "";
       				foreach ($associativeRoom as $key => $value) {
       					$s .= $key." => ".$value.", ";
       				}
       //			error_log("associativeRoom = ".$s);
       				if($primary["class"] == 0) {
       					$associativeClass = idNameMapping("class", "classShortName", "classId", $snapshotId);
       					$primary["class"] = 1;
       				}
       				$s = "";
       				foreach ($associativeClass as $key => $value) {
       					$s .= $key." => ".$value.", ";
       				}
       //			error_log("associativeClass = ".$s);
       				if($primary["subject"] == 0) {
       					$associativeSubject = idNameMapping("subject", "subjectShortName", "subjectId", $snapshotId);
       					$primary["subject"] = 1;
       				}
       				$s = "";
       				foreach ($associativeSubject as $key => $value) {
       					$s .= $key." => ".$value.", ";
       				}
       //				error_log("associativeSubject = ".$s);
       				if($primary["teacher"] == 0) {
       					$associativeTeacher = idNameMapping("teacher", "teacherShortName", "teacherId", $snapshotId);
       					$primary["teacher"] = 1;
       				}
       				$s = "";
       				foreach ($associativeTeacher as $key => $value) {
       					$s .= $key." => ".$value.", ";
       				}
       //				error_log("associativeTeacher = ".$s);
       				if($primary["batch"] == 0) {
       					$associativeBatch = idNameMapping("batch", "batchName", "batchId", $snapshotId);
       					$primary["batch"] = 1;
       				}
       				$s = "";
       				foreach ($associativeBatch as $key => $value) {
       					$s .= $key." => ".$value.", ";
       				}
       //				error_log("associativeBatch = ".$s);
       				$fileName = $table.".json";
       				$file = fopen("uploads/timetableJSON/".$fileName, 'r') or die('Unable to open file!');
       				while(!feof($file)) {
       					$array = [];
       					array_push($array, "null");
       					$tuple = json_decode(fgets($file), true);
       					if(($tuple == NULL) || (empty($tuple) == true)) {
       						continue;
       					}
       					foreach ($tuple as $key => $value) {
       						if($key == "roomShortName") {
       							if(gettype($value) == "NULL")
       								$value = "null";
       							else
       								$value = $associativeRoom[$value];
       						}
       						if($key == "classShortName") {
       							if(gettype($value) == "NULL")
       								$value = "null";
       							else
       								$value = $associativeClass[$value];
       						}
       						if($key == "subjectShortName") {
       							if(gettype($value) == "NULL")
       								$value = "null";
       							else
       								$value = $associativeSubject[$value];
       						}
       						if($key == "teacherShortName") {
       							if(gettype($value) == "NULL")
       								$value = "null";
       							else
       								$value = $associativeTeacher[$value];
       						}
       						if($key == "batchName") {
       							if(gettype($value) == "NULL")
       								$value = "null";
       							else
       								$value = $associativeBatch[$value];
       						}
       						if(gettype($value) == "NULL")
       							$value = "null";
       						array_push($array, $value);
       					}
       					array_push($array, $snapshotId);
       					tableInsert("timeTable", $array, $snapshotId);
       				}
       			}
       //update tertiory table fixedEntry
       			if($table == "fixedEntry") {
       				error_log("Inside tertiory case fixedEntry");
       				$i = 0;
       				$fileName = $table.".json";
       				$file = fopen("uploads/timetableJSON/".$fileName, 'r') or die('Unable to open file!');
       				while(!feof($file)) {
       					$array = [];
       					array_push($array, "null");
       					$query = "select ttId from timeTable where isFixed=1 and snapshotId=3;";
       					$allRows = sqlGetAllRows($query);
       					$values = array();
       					foreach ($allRows as $row) {
       						foreach ($row as $key => $value) {
       							array_push($values, $value);
       						}
       					}
       //					error_log("ttId = ".implode(" ", $values));
       					$tuple = json_decode(fgets($file), true);
       					if(($tuple == NULL) || (empty($tuple) == true)) {
       						continue;
       					}
       					foreach ($tuple as $key => $value) {
       						array_push($array, $values[$i++]);
       						array_push($array, $value);
       					}
       					array_push($array, $snapshotId);
       					tableInsert("fixedEntry", $array);
       				}
       			} //end of case fixedEntry table
       		} //end of looping through each tables
       	} //end of import else
       } //end of import function
    ?>
  </body>
</html>
